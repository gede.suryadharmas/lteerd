<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;

class DTController extends Controller
{
    public function template()
    {
        return view('template');
    }
    public function datatables()
    {
        return view('data-tables');
    }
}